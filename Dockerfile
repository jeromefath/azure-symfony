ARG COMPOSER_VERSION=2
ARG PHP_VERSION=8



FROM php:${PHP_VERSION}-fpm-buster as symfony_php

RUN set -eux; \
    apt-get update && \
    apt-get install -y \
        acl \
        git \
        unzip \
        libzip-dev \
        libicu-dev \
        libpq-dev \
    ;

RUN set -eux; \
	docker-php-ext-install -j$(nproc) \
		zip \
		intl \
		pdo_pgsql \
	; \
	docker-php-ext-enable \
        opcache \
    ;

COPY --from=composer /usr/bin/composer /usr/bin/composer

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="${PATH}:/root/.composer/vendor/bin"

RUN ln -s $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini
COPY docker/php/conf.d/symfony.prod.ini $PHP_INI_DIR/conf.d/symfony.ini

WORKDIR /var/www/html

# prevent the reinstallation of vendors at every changes in the source code
COPY composer.json composer.lock symfony.lock ./
COPY src/Kernel.php src/

RUN set -eux; \
	composer install --prefer-dist --no-dev --no-scripts --no-progress; \
	composer clear-cache

# copy only specifically what we need
COPY .env ./
COPY bin bin/
COPY config config/
COPY migrations migrations/
COPY public public/
COPY src src/
COPY templates templates/
COPY translations translations/

RUN set -eux; \
	mkdir -p var/cache var/log; \
	composer dump-autoload --classmap-authoritative --no-dev; \
    composer dump-env prod; \
    composer run-script --no-dev post-install-cmd; \
    chmod +x bin/console; sync; \
    bin/console assets:install;

VOLUME /var/www/html/var

COPY docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]



FROM debian:buster-slim as symfony_apache

RUN set -eux; \
    apt-get update && \
    apt-get install -y \
        apache2 \
    ;

RUN set -eux; \
    a2enmod \
        proxy_fcgi \
        rewrite \
        deflate \
        headers \
    ;

COPY docker/apache/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY docker/apache/conf-available/symfony.conf /etc/apache2/conf-available/symfony.conf
COPY docker/apache/conf-available/deflate.conf /etc/apache2/conf-available/deflate.conf
COPY docker/apache/conf-available/security.conf /etc/apache2/conf-available/security.conf

RUN ln -sf /dev/stdout /var/log/apache2/access.log && \
    ln -sf /dev/stderr /var/log/apache2/error.log

WORKDIR /var/www/html

COPY public/ /var/www/html/public/

ENTRYPOINT ["/usr/sbin/apache2ctl", "-D" ,"FOREGROUND"]